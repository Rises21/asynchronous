
class Table {
  constructor(tableID,data) {
  	this.tableID = document.getElementById(tableID);
    this.columns = data.columns;
    this.data = data.data;
  }

  createTable(data){
   		//buat tabel
   		let table = document.createElement('table');

   		//buat baris header
   		let headerRow = document.createElement('tr');
      headerRow.classList.add('table');
   		for(let v of this.columns){
   			let headerCell = document.createElement('th');
   			headerCell.appendChild(document.createTextNode(v));
   			headerRow.appendChild(headerCell);
   		}
   		table.appendChild(headerRow);
    
    for(let dataPerson of this.data){
      //console.log(dataPerson,"<<<");
      let dataRow = document.createElement('tr');
      for(let person in dataPerson){
        //console.log(person,">>>>");
       
        let dataText = person == 'id' || person == 'name' || person == 'username' || person == 'email' ? document.createTextNode(dataPerson[person]): null ;

        if ( person == 'address' ) {
                          
              const { street, suite, city } = dataPerson[person];
              let address = `${street}, ${suite}, ${city}.`;
              dataText = document.createTextNode(address);

        } else if ( person == 'company' ) {

              dataText = document.createTextNode(dataPerson[person].name);

        }
        let dataCell = dataText ? document.createElement('td'): null;
        dataText ? dataCell.appendChild(dataText): null;
        dataCell ? dataRow.appendChild(dataCell): null;
      }
      table.appendChild(dataRow);
    }

   this.tableID.appendChild(table);
  }
}

export default Table;

