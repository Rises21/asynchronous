


            // Get the loading spinner element
                let loadingSpinner = document.getElementById("loading");

            


                    


    const getData = articleData => {
            // Hide the loading spinner
                    loadingSpinner.classList.add("d-none");
                    // Clear the news container
                    let newsContainer = document.getElementById("berita");
                    newsContainer.innerHTML = "";


                    // Loop through the articles and add them to the news container
                    articleData.articles.forEach(function(article) {
                    // Create a new card element
                    let card = document.createElement("div");
                    card.classList.add("col-md-3","card");

                    // Create a new card body element
                    let cardBody = document.createElement("div");
                    cardBody.classList.add("card-body");

                    // Create a new card title element
                    let cardTitle = document.createElement("h5");
                    cardTitle.classList.add("card-title");
                    cardTitle.innerText = article.title;

                    // Create a new card text element
                    let cardText = document.createElement("p");
                    cardText.classList.add("card-text");
                    cardText.innerText = article.description;

                    // Create a new card text element published
                    let cardTextPublished = document.createElement("p");
                    cardTextPublished.classList.add("card-text-published");
                    let publishDate = new Date();
                        publishDate.setTime(Date.parse(article.publishedAt));

                        const weekday = ["Minggu","Senin","Selasa","Rabu","Kamis","Jum'at","Sabtu"];
                    let day = weekday[publishDate.getDay()];
                    let date = publishDate.getDate();
                    let month = publishDate.getMonth();
                    let year = publishDate.getFullYear();
                    
                    function addZero(i) {
                              if (i < 10) {i = "0" + i}
                              return i;
                            }   
                            let h = addZero(publishDate.getHours());
                            let m = addZero(publishDate.getMinutes());
                            let s = addZero(publishDate.getSeconds());                   
                    let time = h + ":" + m + ":" + s;

                    cardTextPublished.innerText = `${day} (${date}/${month}/${year})  ${time}`;

                    // Create a new card link element
                    let cardLink = document.createElement("a");
                    cardLink.classList.add("btn", "btn-primary");
                    cardLink.innerText = "Read More";
                    cardLink.href = article.url;

                    // Append the card title, card text, card text published, and card link to the card body
                    cardBody.appendChild(cardTitle);
                    cardBody.appendChild(cardText);
                    cardBody.appendChild(cardTextPublished);
                    cardBody.appendChild(cardLink);

                    // Create a new card image element
                    let cardImage = document.createElement("img");
                    cardImage.classList.add('imgBerita');
                    cardImage.src = article.urlToImage;
                    cardImage.alt = article.title;


                    // Append the card image and card body to the card
                    card.appendChild(cardImage);
                    card.appendChild(cardBody);

                    // Append the card to the news container
                    newsContainer.appendChild(card);

                    
                  
                });
    };


   

 // Send a request to the API
      
        let articlePromise = new Promise((resol,rejec) => {

        let url = 'https://newsapi.org/v2/top-headlines?' +
                  'country=id&' +
                  'apiKey=989548329dd94e968d3d9779c9310de3';
        let req = new Request(url);
        fetch(req).then((response)=>{
        
            if (response.status == 200) {
                                            
                              resol(response.json());

                            } else {
                              rejec("File not Found");
                            }
            });

        });
  
articlePromise.then(
        function(value) {
            getData(value);
        },
        function(error) {getData(error);}
        );

  // When the search button is clicked or you can setting to keyup for livesearch
            //search.addEventListener("click", function(){...} for using button search
                search.addEventListener("keyup", function() { 

             // Get the search button element
                let searchBtn = document.getElementById("searchBtn");
            // Get the search input element
                let searchInput = document.getElementById("search");
                    console.log(searchInput.value,"<<<<");
            // Get the value of the search input
                let query = searchInput.value;
            // If the search input is not empty
                if (query !== "") {
                        loadingSpinner.classList.remove("d-none");
                        } else {

                           return articlePromise.then(
                                function(value) {
                                    getData(value);
                                },
                                function(error) {getData(error);}
                                );
                        }

        // Send a request to the API again
      
        let articlePromiseSearch = new Promise((resol,rejec) => {
             
        let url = 'https://newsapi.org/v2/everything?' +
          `q=${query}&` +
          'from=2023-02-17&' +
          'sortBy=popularity&' +
          'apiKey=989548329dd94e968d3d9779c9310de3';
        let req = new Request(url);
        fetch(req).then((response)=>{

            if (response.status == 200) {
                                  loadingSpinner.classList.remove("d-none");
            console.log(loadingSpinner,">>>>>");
                              resol(response.json());

                            } else {
                              rejec("File not Found");
                            }
            });

        });

        articlePromiseSearch.then(
        function(value) {
            getData(value);
        },
        function(error) {getData(error);}
        );

});


